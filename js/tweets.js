$(document).ready(function(){
	$(".favourite").click(function(e){
		e.preventDefault();
		$.ajax({
			url:'tweeter_activites.php',
			type:'post',
			data:{'id':$(this).attr('tweet_id'),'method':'favourite'},
			success:function(response){
			responseData = JSON.parse(response);
			if (typeof responseData.errors == "undefined")
				{
				 $('#modal .modal-body').html("<p>The Tweet Successfully added to your Favourits</p>");
                 $('#modal').modal('show');
				}
			else
				{
				$('#modal .modal-body').html("<p>" + responseData.errors[0].message+ " </p>");
                $('#modal').modal('show');
				}
			}
		});
	});
	$(".retweet").click(function(e){
		e.preventDefault();
		$.ajax({
			url:'tweeter_activites.php',
			type:'post',
			data:{'id':$(this).attr('tweet_id'),'method':'retweet'},
			success:function(response){
			responseData = JSON.parse(response);
			if (typeof responseData.errors == "undefined")
				{
				 $('#modal .modal-body').html("<p>The Tweet Successfully retweeted</p>");
                 $('#modal').modal('show');
				}
			else
				{
					$('#modal .modal-body').html("<p>" + responseData.errors[0].message+ " </p>");
	                 $('#modal').modal('show');
				}
			}
		});
	});
	
	$('#myModal').on('show.bs.modal', function(e) {

		var tweetId = $(e.relatedTarget).data('tweet-id');
	    $(e.currentTarget).find('textarea').attr("tweetId",tweetId);
	    $(e.currentTarget).find('textarea').val("");
	});
	$("#tweethis").submit(function(event){
		event.preventDefault();
		$.ajax({
			url:'tweeter_activites.php',
			type:'post',
			data:{'id':$("textarea").attr('tweetId'),'method':'reply','tweet':$("textarea").val()},
			success:function(response){
			responseData = JSON.parse(response);
			if (typeof responseData.errors == "undefined")
				{
				 $(".confirm").css("display","block");
				}
			else
				{
					 $('#modal .modal-body').html("<p>" + responseData.errors[0].message+ " </p>");
	                 $('#modal').modal('show');
				}
			}
			});
		return false;
		});
	$('body').on('hidden.bs.modal', '#myModal', function() {
		  $(this).removeData('bs.modal');
		});
	});



